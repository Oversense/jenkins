FROM maven:3-jdk-8
RUN apt-get update; \
    apt-get install git -y; \
    apt-get install docker.io -y; \
    apt-get install nginx -y;
CMD ["nginx", "-g", "daemon off;"]
